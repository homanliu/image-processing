#include <opencv2/opencv.hpp>
#include <math.h>
#include <iostream>
#include <stdlib.h>
#include <pthread.h>
#include <string>
#include <functional>
#define IMAGE_FORMAT uchar

#if defined(__WIN32__) || defined(__WIN64__)
   const char pathSlash = '\\';
#elif defined(__linux__) || defined(__APPLE__)
   const char pathSlash = '/';
#endif

using namespace std;
using namespace cv;

cv::Mat rawImage, imageForBall;

typedef struct {
	cv::Point centre;
	int distance;
    int maxRadius;
    int depthPos;
} circleInfo;

typedef struct {
    cv::Point3i point1;
    cv::Point3i point2;
    cv::Point3d intersect;
    double twoPointDistance;
    double z_interpolation;
} intersectionPoint;

// Directories name which are stored images
const char* highestRodDir = "highestRod";
const char* sampleImageDir = "sampleImage";
const char* boundaryCaseDir = "boundaryCase";
const char* imageDir = "throwing-ball";

// debug:       31 - 212 
// debug-image: 1 - 83
// image-set:   1 - 127
// image-set2:  1 - 220
const char* debugDir = "image-set";
const int imageStart = 1;
const int imageEnd = 127;

// Here we use millimeter
const int fenceHeight = 2500;
// Kinect height may be changed
const int kinectHeight = 1300;
// Smallest value here
const int fenceToKinect = 3000;
const int tolerance = 200;
const int maxDepthRange = 8000;
const int rodLength = 35;
const int rodWidth = 7;

int imageNumber = 1;
const int medianBlurValue = 5;
const int cannyLower = 10;
const int cannyUpper = 230;
const int maxTZ1Height = 195;

// Global results of Goal and Ball tracing
int detectedBall = 0, recordedPos = 0, zPos = -1;
cv::Point3i ballPath[20];
circleInfo outputCircle;
bool circleExist = false;

pthread_t ballTracking;
pthread_mutex_t mutexLock = PTHREAD_MUTEX_INITIALIZER;

class Queue{
	public:
		Queue(int inputSize){
            // Constructor
			validElement = 0;
			circleArray = new circleInfo[inputSize];
			for(int i=0; i<inputSize; i++){
				circleArray[i].centre.x = -1;
				circleArray[i].centre.y = -1;
				circleArray[i].distance = -1;
                circleArray[i].depthPos = -1;
			}
			arraySize = inputSize;
		}
        
		void enqueue(cv::Point input, int maxRadius, int depthPos){
            // Dequeue and enqueue input element if full
			if(validElement < arraySize){
				circleArray[validElement].centre.x = input.x;
				circleArray[validElement].centre.y = input.y;
				circleArray[validElement].distance = pow(input.x, 2) + pow(input.y, 2);
                circleArray[validElement].maxRadius = maxRadius;
                circleArray[validElement].depthPos = depthPos;
				validElement += 1;
			}
			else{
				for(int i=1; i<arraySize; i++){
					circleArray[i - 1].centre.x = circleArray[i].centre.x;
					circleArray[i - 1].centre.y = circleArray[i].centre.y;
					circleArray[i - 1].distance = circleArray[i].distance;
                    circleArray[i - 1].maxRadius = circleArray[i].maxRadius;
                    circleArray[i - 1].depthPos = circleArray[i].depthPos;
				}
				circleArray[arraySize - 1].centre.x = input.x;
				circleArray[arraySize - 1].centre.y = input.y;
				circleArray[arraySize - 1].distance = pow(input.x, 2) + pow(input.y, 2);
                circleArray[arraySize - 1].maxRadius = maxRadius;
                circleArray[arraySize - 1].depthPos = depthPos;
			}
		}

		circleInfo medianDistance(){
			int index;
			if(validElement == 1)
				return circleArray[validElement - 1];

            // Sort according to centre distance with (0, 0) and radius
			int *tempDistance = new int[validElement];
			for(int i=0; i<validElement; i++)
				tempDistance[i] = circleArray[i].distance * 1000 + circleArray[i].maxRadius;
			sort(tempDistance, tempDistance + validElement);
            // cout << "List: " << tempDistance << endl;

            // Take most reasonable one to be centre
            int desiredList[(validElement / 2)], desiredValue;
            for(int i=0; i<(validElement / 2); i++)
                desiredList[i] = tempDistance[int(validElement / 4) + i];
            for(int i=0; i<(validElement / 2); i++){
                if(abs((desiredList[i] % 1000) - (desiredList[(i + 1) % (validElement / 2)] % 1000)) < 2){
                    if(desiredList[i] > desiredList[(i + 1) % (validElement / 2)])
                        desiredValue = desiredList[i];
                    else
                        desiredValue = desiredList[(i + 1) % (validElement / 2)];
                }
            }

            // cout << "Desired Value: " << desiredValue << endl;

			for(index=0; index<validElement; index++){
                // cout << "Calculated " << index << ": " << circleArray[index].distance * 1000 + circleArray[index].maxRadius << endl;
				if(circleArray[index].distance * 1000 + circleArray[index].maxRadius == desiredValue)
					break;
            }
			
            delete[] tempDistance;
			return circleArray[index];
		}

        void deleteAllocation(){
            delete[] circleArray;
        }

	private:
		circleInfo *circleArray;
		int validElement;
		int arraySize;
};

int arraySize = 10;
Queue *locationQueue = new Queue(arraySize);

// Only for image inputs in defined paths
char* pathParser(const char *fileDir){
    char *parsedPath;
    char buffer[10];
    int pathLength = 0;
    // Here we define image path as "sampleImage/test1-4500.png"
    pathLength = strlen(fileDir) + 1 + strlen("image") + int(log(imageNumber)) + 6 + strlen(".png");
    parsedPath = new char[pathLength + 1];

    for(int i=0; i<strlen(fileDir); i++)
        parsedPath[i] = fileDir[i];
    
    parsedPath[strlen(fileDir)] = pathSlash;
    parsedPath[strlen(fileDir)+1] = '\0';
    
    std::strcat(parsedPath, "image");
    sprintf(buffer,"%d",imageNumber);
    std::strcat(parsedPath, buffer);
    std::strcat(parsedPath, ".png");

    parsedPath[pathLength] = '\0';
    return parsedPath;
}

// Calculate minimum meaningful colour range
void colorCalculation(int *lowerColor){
    // Pythagoras's theorem, a^2 + b^2 = c^2
    double centreToKinect = sqrt(pow(fenceHeight - kinectHeight, 2) + pow(fenceToKinect, 2));
    double lowerDistance = centreToKinect - tolerance;
    // Map from depth value to grayscale, change here if using raw 16 bits
    *lowerColor = int(255.0 * lowerDistance/maxDepthRange);
}

int getRodCoordinates(int imageWidth, int **whitePoints, int pointCount){
    // Initialize array with all zero to store satisfied x-coordinates
    int *rowWhiteNumber = new int[imageWidth];
    int rowWhiteCount = 0, yCoordinates = 0;
    for(int i=0; i<10; i++)
        rowWhiteNumber[i] = -1;

    int currentX = 0, consecutiveCount = 0, lastHeight = 0;
    bool chosen = false, garbage = false;
    // Finding x-coordinate of inner circle in set of white points
    for(int i=0; i<pointCount; i++){
        // All points are already sorted according to x-coordinate
        if(whitePoints[0][i] < imageWidth / 2){
            currentX = 0;
            continue;
        }
        if(currentX != whitePoints[1][i]){
            // Re-count if x-coordinate changed
            currentX = whitePoints[1][i];
            lastHeight = whitePoints[0][i];
            consecutiveCount = 0;
            chosen = false;
            garbage = false;
        }
        else{
            // Give tolerance since poor accuracy for long distance
            if(whitePoints[0][i] - lastHeight <= 2 && !garbage){
                // Keep counting if we detected consecutive white
                consecutiveCount += 1;
                lastHeight = whitePoints[0][i];
            }
            // else
            //     garbage = true;
            // Large consecutive count means that it is probably part of rod
            if(consecutiveCount >= rodLength && !chosen){
                // Mark it and denoted as chosen to prevent re-adding
                rowWhiteNumber[rowWhiteCount] = currentX;
                rowWhiteCount += 1;
                chosen = true;
            }
        }
    }

    if(rowWhiteCount > 0){
        // New Algorithm: Find width of all objects in line and ignore unreasonable width
        int temp = 1;
        for(int i=temp; i<rowWhiteCount; i++){
            if(rowWhiteNumber[i] - rowWhiteNumber[i-1] != 1 || (i == rowWhiteCount - 1 && rowWhiteNumber[i] - rowWhiteNumber[i-1] == 1)){
                // Numbers represent pixels, so width has to +1
                int width = rowWhiteNumber[i-1] - rowWhiteNumber[temp-1] + 1;
                if(i == rowWhiteCount - 1 && rowWhiteNumber[i] - rowWhiteNumber[i-1] == 1)
                    width = rowWhiteNumber[i] - rowWhiteNumber[temp-1] + 1;
                if(width > 1 && width <= rodWidth){
                    // Simply take middle one in the consecutive numbers
                    yCoordinates = rowWhiteNumber[temp - 1 + int(width/2)];

                    // Free used objects to prevent overflow
                    delete[] rowWhiteNumber;
                    return yCoordinates;
                }
                else{
                    temp = i + 1;
                }
            }
        }
        // Free used objects to prevent overflow
        delete[] rowWhiteNumber;
        return -1;
    }
    else{
        delete[] rowWhiteNumber;
        return -1;
    }
}

int getCircleCoordinates(int imageHeight, int **whitePoints, int pointCount, int centreX){
    int listOfHeight = 0, countList = 0, centreY = 0, threshold = 15;
    bool blackZone = false;
    for(int i=imageHeight; i>=0; i--){
        int count = 0;
        for(int j=0; j<pointCount; j++){
            // Test every points if they are inside circle, Increase count
            int dY = pow((whitePoints[0][j] - i), 2);
            int dX = pow((whitePoints[1][j] - centreX), 2);
            // Circle that must have smaller radius than inner one
            if(dX + dY < pow(15, 2))
                count += 1;
        }
        // If we get enough amounts of count, it means that now it is looping in the black area
        if(countList > threshold)
            blackZone = true;
        // Once we get a circle with too many white points, then we stop looping
        if(count > threshold && blackZone)
            break;
        else if(count <= threshold){
            listOfHeight += i;
            countList += 1;
        }
    }
    // Take sum of average to get y-coordinate of centre
    if(countList > 0){
        centreY = int(listOfHeight / countList);
        return centreY;
    }
    else
        return -1;
}

int getCircleRadius(int **whitePoints, int pointCount, cv::Point centre){
    // Test for the maximum acceptable radius
    int largestRadius = 0, threshold = 15;
    // 50 can be other values which is sufficiently large enough
    for(int i=0; i<50; i++){
        int count = 0;
        for(int j=0; j<pointCount; j++){
            // Test every points if they are inside circle, Increase count
            int dY = pow((whitePoints[0][j] - centre.y), 2);
            int dX = pow((whitePoints[1][j] - centre.x), 2);
            if(dX + dY < pow(i, 2))
                count += 1;
        }
        // Once we get a circle with too many white points, then we stop looping
        if(count > threshold)
            break;
        // Keep storing largest radius value
        else if(count <= threshold && i > largestRadius){
            largestRadius = i;
        }
    }
    if(largestRadius > 0)
        return largestRadius;
    else
        return -1;
}

// Draw function for finding inner circle
void drawBoundingArea(cv::Mat rawImage, cv::Mat rodImage, int **whitePoints, int pointCount){
    // Centre of bounding circle (x, y)
    cv::Point centre;

    if (pointCount == 0) {
		cout << "Not found" << endl;
	}
    
    centre.x = getRodCoordinates(rodImage.rows, whitePoints, pointCount);
    if(centre.x == -1){
        cout << "Cannot locate rod" << endl;
    }

    // int rodDepth = 0, count = 0;
    // for (int i = 8; i >= 0; i--){
    //     int tempDepth = int(rodImage.at<IMAGE_FORMAT>(rodImage.rows/2 - i, centre.x));
    //     if (tempDepth > 0){
    //         rodDepth += tempDepth;
    //         count += 1;
    //     }
    // }
    // zPos = rodDepth / count;

    centre.y = getCircleCoordinates(rodImage.cols / 2, whitePoints, pointCount, centre.x);
    if(centre.y == -1){
        cout << "Cannot find centre" << endl;
    }

    int largestRadius = getCircleRadius(whitePoints, pointCount, centre);
	if (largestRadius == -1) {
		cout << "Cannot find radius" << endl;
	}
    
    int depthPos = -1;
    // Locate z-Position for goal circle
    for(int j=0; j<pointCount; j++){
        // Test every points if they are on circle
        int dY = pow((whitePoints[0][j] - centre.y), 2);
        int dX = pow((whitePoints[1][j] - centre.x), 2);
        if(abs(dX + dY - pow(largestRadius, 2)) < 50){
            int tempPosition = int(rodImage.at<IMAGE_FORMAT>(whitePoints[0][j], whitePoints[1][j]));
            if(tempPosition > depthPos)
                depthPos = tempPosition;
        }
    }
    
    if(centre.x > 0 && centre.y > 0 && largestRadius > 0 && depthPos > 0)
        locationQueue->enqueue(centre, largestRadius, depthPos);
    outputCircle = locationQueue->medianDistance();

	// Draw circle in raw image instead of processed image
    if(outputCircle.centre.x > 0 && outputCircle.centre.y > 0 && outputCircle.maxRadius > 0 && outputCircle.depthPos > 0){
        circleExist = true;
        circle(rawImage, outputCircle.centre, outputCircle.maxRadius, CV_RGB(255, 255, 255), 2);
        zPos = outputCircle.depthPos;
        cout << "Rod Depth: " << zPos << endl;
    }
}

void preFiltering(cv::Mat rawImage, cv::Mat rodImage, int lowerColorRange){
    int *whitePoints[2], pointCount = 0;
    // Reset to -1 since it will be updated each frame
    outputCircle.centre = cv::Point(-1, -1);
    outputCircle.maxRadius = -1;
    // whitePoints[0] = set of y-coordinates
    whitePoints[0] = new int[100000];
    // whitePoints[1] = set of x-coordinates
    whitePoints[1] = new int[100000];
    // Filter out unrelated pixels, change here if using raw 16 bits
    for(int j=0; j<rodImage.cols; j++){
        for(int i=0; i<rodImage.rows; i++){
            // Assume that the circle must be higher than image centre
            if(i >= rodImage.cols/2)
                rodImage.at<IMAGE_FORMAT>(i, j) = 0;
            // Trim out leftmost and rightmost 1/8 image to reduce noise
            else if(j <= rodImage.rows/8)
                rodImage.at<IMAGE_FORMAT>(i, j) = 0;
            else if(j >= rodImage.rows* 7/8)
                rodImage.at<IMAGE_FORMAT>(i, j) = 0;
            // Set all smaller than minimum colour value points to zero
            else if(rodImage.at<IMAGE_FORMAT>(i, j) <= lowerColorRange)
                rodImage.at<IMAGE_FORMAT>(i, j) = 0;
            else{
                // Set it to white and add to array for faster calculation
                if(pointCount < 100000){
                whitePoints[0][pointCount] = i;
                whitePoints[1][pointCount] = j;
                pointCount += 1;
            }
        }
    }
    }
    // Keep processing if there is at least one point
    if(pointCount > 0)
        drawBoundingArea(rawImage, rodImage, whitePoints, pointCount);
    delete[] whitePoints[0];
    delete[] whitePoints[1];
}

cv::Point3d circle_line_intersection(int circle_radius, cv::Point circle_centre, cv::Point3i point2, cv::Point3i point1, bool return_point1){
    // Reference: https://math.stackexchange.com/questions/228841/how-do-i-calculate-the-intersections-of-a-straight-line-and-a-circle
    cv::Point3d output_point = cv::Point3d(-1.0, -1.0, -1.0);

    double m, c, p, q, r, A, B, C, delta;
    p = circle_centre.x;
    q = circle_centre.y;
    r = circle_radius;
    if (point2.x - point1.x == 0)
        return output_point;
    // y = mx + c
    m = (point2.y - point1.y) / (point2.x - point1.x);
    c = point1.y - m * point1.x;

    A = 1 + pow(m, 2);
    B = 2.0 * (m * c - m * q - p);
    C = pow(q, 2) - pow(r, 2) + pow(p, 2) - (2.0 * c * q) + pow(c, 2);

    delta = pow(B, 2) - 4.0 * A * C;

    if (delta >= 0){
        cv::Point3d intersect_point1, intersect_point2;

        intersect_point1.x = (-B + sqrt(delta)) / (2 * A);
        intersect_point2.x = (-B - sqrt(delta)) / (2 * A);

        intersect_point1.y = m * intersect_point1.x + c;
        intersect_point2.y = m * intersect_point2.x + c;

        // Reference: https://math.stackexchange.com/questions/35857/two-point-line-form-in-3d
        double xz_slope = double(point2.z - point1.z) / (point2.x - point1.x);
        intersect_point1.z = xz_slope * (intersect_point1.x - point1.x) + point1.z;
        intersect_point2.z = xz_slope * (intersect_point2.x - point1.x) + point1.z;

           if(return_point1 == true)
            return intersect_point1;
        else
            return intersect_point2;
    }
    return output_point;
}

bool point_in_line_segment(cv::Point3d point, cv::Point3i line_point1, cv::Point3i line_point2){
    // Reference: https://stackoverflow.com/questions/328107/how-can-you-determine-a-point-is-between-two-other-points-on-a-line-segment
    double threshold = 0.3;
    double line_distance = sqrt(pow(line_point2.x - line_point1.x, 2) + pow(line_point2.y - line_point1.y, 2));

    double distance_with_point1 = sqrt(pow(point.x - line_point1.x, 2) + pow(point.y - line_point1.y, 2));
    double distance_with_point2 = sqrt(pow(point.x - line_point2.x, 2) + pow(point.y - line_point2.y, 2));

    if (distance_with_point1 + distance_with_point2 - line_distance < threshold)
        return true;
    else
        return false;
}

void goalDetection(){
    bool result = false;
    int pointCount = 0;
    intersectionPoint *pointsOnLine = new intersectionPoint[3];

    // Case 1: Get two intersection points for path
    for(int i=1; i<recordedPos; i++){
        // The ball should fly with increasing z-distance
        // cout << ballPath[i].z << endl;
        if(ballPath[i].z - ballPath[i - 1].z < 0)
            result = false;

        // At least one point intersection between circle and path only considering xy-plane
        cv::Point3d intersect1, intersect2;
        intersect1 = circle_line_intersection(outputCircle.maxRadius, outputCircle.centre, ballPath[i - 1], ballPath[i], true);
        intersect2 = circle_line_intersection(outputCircle.maxRadius, outputCircle.centre, ballPath[i - 1], ballPath[i], false);

        // Check the solutions are within line segments or not
        if(point_in_line_segment(intersect1, ballPath[i - 1], ballPath[i]) == true && intersect1.z != -1.0){
            // Display on screen
            cv::line(rawImage, cv::Point(intersect1.x - 5, intersect1.y), cv::Point(intersect1.x + 5, intersect1.y), Scalar(255, 0, 0), 2);
            cv::line(rawImage, cv::Point(intersect1.x, intersect1.y - 5), cv::Point(intersect1.x, intersect1.y + 5), Scalar(255, 0, 0), 2);
            // Return only points lied within line segments
            pointsOnLine[pointCount].point1 = ballPath[i - 1];
            pointsOnLine[pointCount].point2 = ballPath[i];
            pointsOnLine[pointCount].intersect = intersect1;
            pointCount += 1;
        }
        if(point_in_line_segment(intersect2, ballPath[i - 1], ballPath[i]) == true && intersect1.z != -1.0){
            // Display on screen
            cv::line(rawImage, cv::Point(intersect2.x - 5, intersect2.y), cv::Point(intersect2.x + 5, intersect2.y), Scalar(255, 0, 0), 2);
            cv::line(rawImage, cv::Point(intersect2.x, intersect2.y - 5), cv::Point(intersect2.x, intersect2.y + 5), Scalar(255, 0, 0), 2);
            // Return only points lied within line segments
            pointsOnLine[pointCount].point1 = ballPath[i - 1];
            pointsOnLine[pointCount].point2 = ballPath[i];
            pointsOnLine[pointCount].intersect = intersect2;
            pointCount += 1;
        }
    }

    if(pointCount >= 2 && recordedPos >= 2){
        cout << "Intersection Point 1: " << pointsOnLine[0].intersect << endl;
        cout << "Rod Position: " << zPos << endl;
        cout << "Intersection Point 2: " << pointsOnLine[pointCount - 1].intersect << endl;

        if(floor(pointsOnLine[0].intersect.z) <= zPos && zPos <= ceil(pointsOnLine[pointCount - 1].intersect.z)){
            cout << "Cause by two points to return goal" << endl;
            result = true;
        }
        else{
            cout << "Cause by two points to return fail" << endl;
        }
    }

    // Case 2: Get only one intersection points for path
    else if(pointCount <= 1 && recordedPos >= 2){
        cv::Point3d intersect1_last_line, intersect2_last_line;
        intersect1_last_line = circle_line_intersection(outputCircle.maxRadius, outputCircle.centre, ballPath[recordedPos - 2], ballPath[recordedPos - 1], true);
        intersect2_last_line = circle_line_intersection(outputCircle.maxRadius, outputCircle.centre, ballPath[recordedPos - 2], ballPath[recordedPos - 1], false);

        cv::Point3d intersect1, intersect2;
        if (pointCount == 1)
            intersect1 = pointsOnLine[0].intersect;
        else{
            intersect1 = (intersect1_last_line.z < intersect2_last_line.z) ? intersect1_last_line : intersect2_last_line;

            cv::line(rawImage, cv::Point(intersect1.x - 5, intersect1.y), cv::Point(intersect1.x + 5, intersect1.y), Scalar(0, 0, 255), 2);
            cv::line(rawImage, cv::Point(intersect1.x, intersect1.y - 5), cv::Point(intersect1.x, intersect1.y + 5), Scalar(0, 0, 255), 2);
        }

        intersect2 = (intersect1_last_line.z > intersect2_last_line.z) ? intersect1_last_line : intersect2_last_line;

        // Draw intersections
        cv::line(rawImage, cv::Point(intersect2.x - 5, intersect2.y), cv::Point(intersect2.x + 5, intersect2.y), Scalar(0, 0, 255), 2);
        cv::line(rawImage, cv::Point(intersect2.x, intersect2.y - 5), cv::Point(intersect2.x, intersect2.y + 5), Scalar(0, 0, 255), 2);

        cout << "Intersection Point 1: " << intersect1 << endl;
        cout << "Rod Position: " << zPos << endl;
        cout << "Intersection Point 2: " << intersect2 << endl;

        if (floor(intersect1.z) <= zPos && zPos <= ceil(intersect2.z)) {
            cout << "Cause by one or zero point to return goal" << endl;
            result = true;
        }
        else{
            cout << "Cause by one or zero point to return fail" << endl;
        }
    }

    // Put text on displayed image
	if (result == true && recordedPos > 0) {
		putText(rawImage, string("Goal"), Point(430, 30), 0, 1, Scalar(0, 127, 255), 2);
		cout << "GOAL" << endl;
	}
	else if (result == false && recordedPos > 0) {
		putText(rawImage, string("Fail"), Point(430, 30), 0, 1, Scalar(0, 127, 255), 2);
		cout << "FAIL" << endl;
	}
    delete[] pointsOnLine;
}

bool point_in_circle(cv::Point centre, int radius, cv::Point test_point){
    int dx = abs(test_point.x - centre.x);
    int dy = abs(test_point.y - centre.y);

    if (dx > radius || dy > radius)
        return false;

    if (dx + dy <= radius)
        return true;

    if (pow(dx, 2) + pow(dy, 2) <= pow(radius, 2))
        return true;
    else
        return false;
}

void ballFilter(){
    cv::Mat cannyEdge, temp;
	int areaThreshold = 0;
    vector<vector<cv::Point> > contours;
    vector<Vec4i> hierarchy;

	if (circleExist == false)
		return;

    // Trim out lower half of image
    for(int j=0; j<imageForBall.cols; j++){
        for(int i=0; i<imageForBall.rows; i++){
            if(j > outputCircle.centre.x - 10 && j < outputCircle.centre.x + 10 && i > outputCircle.centre.y + outputCircle.maxRadius + 10)
                imageForBall.at<IMAGE_FORMAT>(i, j) = 0;
            // Assume that the ball must be higher than image centre, change here if using raw 16 bits
            if(i >= imageForBall.rows/2)
                imageForBall.at<IMAGE_FORMAT>(i, j) = 0;
            if(!point_in_circle(outputCircle.centre, outputCircle.maxRadius, cv::Point(j, i)) && point_in_circle(outputCircle.centre, outputCircle.maxRadius + 18, cv::Point(j, i)))
                imageForBall.at<IMAGE_FORMAT>(i, j) = 0;
        }
    }

    // Function(sourceImage, destImage, params);
    cv::imshow("Image Trimmed", imageForBall);
    medianBlur(imageForBall, temp, 2 * medianBlurValue + 1);
    Canny(temp, cannyEdge, cannyLower, cannyUpper);
    cv::imshow("Median Blur", temp);
    findContours(cannyEdge, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

    // Draw all contours with filled colour
    int maxContour = 0;
    Scalar color(255, 0, 0);
    for(int i = 0; i < contours.size(); i++){ // Iterate through each contour
		if (contourArea(contours[i]) > areaThreshold) {
			drawContours(rawImage, contours, i, color, CV_FILLED, 8, hierarchy);
			cout << "Area: " << contourArea(contours[i]) << endl;
		}
        if(i > 0){
            if(contourArea(contours[i]) > contourArea(contours[maxContour]))
                maxContour = i;
        }
    }

    if(contours.size() > 0){
		vector<vector<Point> > contours_poly(contours.size());
		vector<Rect> boundRect(contours.size());
		vector<Point2f>center(contours.size());
		vector<float>radius(contours.size());
        // Get minimum ellipse of contours
        for(int i = 0; i < contours.size(); i++){
			if (contourArea(contours[i]) > areaThreshold) {
				approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
				boundRect[i] = boundingRect(Mat(contours_poly[i]));
				minEnclosingCircle((Mat)contours_poly[i], center[i], radius[i]);
			}
        }
        
        // Draw centre on image
        // cout << "{ " << massCentre[0].x << ", " << massCentre[0].y << " }" << endl;
        // cv::line(rawImage, cv::Point(massCentre[0].x - 5, massCentre[0].y), cv::Point(massCentre[0].x + 5, massCentre[0].y), Scalar(255, 255, 0), 2);
        // cv::line(rawImage, cv::Point(massCentre[0].x, massCentre[0].y - 5), cv::Point(massCentre[0].x, massCentre[0].y + 5), Scalar(255, 255, 0), 2);

        // Record current first point to vector array
		if (center[maxContour].x > 0 && center[maxContour].y > 0 && recordedPos < 20) {
			ballPath[recordedPos].x = center[maxContour].x;
            ballPath[recordedPos].y = center[maxContour].y;
			ballPath[recordedPos].z = (int)imageForBall.at<IMAGE_FORMAT>(int(center[maxContour].y), int(center[maxContour].x));
            putText(rawImage, string(std::to_string(ballPath[recordedPos].z)), Point(40, 30), 0, 1, Scalar(0, 127, 255), 2);
			recordedPos += 1;
			detectedBall = 1;
		}
		else if (recordedPos == 20 && center[maxContour].x > 0 && center[maxContour].y > 0) {
			recordedPos = 0;
			ballPath[recordedPos].x = center[maxContour].x;
            ballPath[recordedPos].y = center[maxContour].y;
			ballPath[recordedPos].z = (int)imageForBall.at<IMAGE_FORMAT>(int(center[maxContour].y), int(center[maxContour].x));
            putText(rawImage, string(std::to_string(ballPath[recordedPos].z)), Point(40, 30), 0, 1, Scalar(0, 127, 255), 2);
			recordedPos += 1;
			detectedBall = 1;
		}
		else {
			detectedBall -= 1;
		}
    }
    else{
        detectedBall -= 1;
    }

    // Reset vector array if cannot detect ball in consecutive 5 frames
    // if(detectedBall == -4){
    //     recordedPos = 0;
    // }

    // Draw trace line with mutex lock to achieve mutually exclusive
    for(int i=1; i<recordedPos; i++)
        cv::line(rawImage, cv::Point(ballPath[i-1].x, ballPath[i-1].y), cv::Point(ballPath[i].x, ballPath[i].y), Scalar(0, 255, 255), 2);
	cannyEdge.release();
	temp.release();
}

void imageProcessing(cv::Mat rodImage, int lowerColorRange){
    rodImage.copyTo(imageForBall);

    // Create thread to perform two separated tasks
    circleExist = false;
    preFiltering(rawImage, rodImage, lowerColorRange);
	ballFilter();

	if (detectedBall == -3 && recordedPos > 0) {
		cout << "Goal Detection is running" << endl;
		goalDetection();
        recordedPos = 0;
	}

    zPos = -1;
}

// const int medium_blur_max = 7;
// int alpha_slider;

// void on_trackbar( int, void* )
// {
//   cout << "Trackbar: " << alpha_slider << endl;
// }

// Get a filtered image for finding bounding circle
void imageFopen(char *filePath, int lowerColorRange){
    cv::Mat rodImage;
	rawImage = imread(filePath, 1);
    rawImage.convertTo(rawImage, CV_8U, 10.0f);

    pthread_mutex_init(&mutexLock, NULL);

	if (!rawImage.data)
		cout << "Cannot find " << filePath << endl;
	else{
        // After opening files, convert to greyscale and pass to processing function
        // Here is only for reading image since image is stored in RGBA
        cvtColor(rawImage, rodImage, COLOR_BGR2GRAY);
        imageProcessing(rodImage, lowerColorRange);

        // char TrackbarName[50];
        // sprintf(TrackbarName, "Medium Blur", medium_blur_max);
        // createTrackbar( TrackbarName, "Test", &alpha_slider, medium_blur_max, on_trackbar);
        // on_trackbar(alpha_slider, 0);

		imshow("Test", rawImage);
		cvWaitKey(0);
	}
    rawImage.release();
    imageForBall.release();
    rodImage.release();
}

int main(int argc, char **argv){
    int lowerColor;
    colorCalculation(&lowerColor);
    imageNumber = imageStart;
    for(int i=0; i<imageEnd - imageStart + 1; i++){
        char* filePath = pathParser(debugDir);
        cout << filePath << endl;
        imageFopen(filePath, lowerColor);
        imageNumber += 1;
        cout << "---------------------" << endl;
        delete[] filePath;
    }
    locationQueue->deleteAllocation();
    delete locationQueue;
    return 0;
}